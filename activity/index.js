console.log("hello world");
let num = 9
const getCube = Math.pow(num, 3);
message = `The Cube of ${num} is ${getCube}`;
console.log(message);

address = [19, "Green Street", "Quezon City"];
const [ streetNum, streetName, cityName ] = address;
console.log(`I live at ${streetNum} ${streetName} ${cityName}!`);

let animal = {
givenName: "Lolong",
animalName: "Salt Water Crocodile",
weight: 1075,
measureFeet: 20,
measureInches: 3,
};
const {givenName, animalName, weight, measureFeet, measureInches} = animal;
console.log(`${animal.givenName} was a ${animal.animalName}. He weighed at ${animal.weight} kgs. With a measurement of ${animal.measureFeet} ft ${animal.measureInches} in.`);

const nums = [1, 2, 3, 4, 5, 15];
nums.forEach(x => console.log(x));

class dog{
constructor (name, age, breed){
this.name = name,
this.age = age,
this.breed = breed
}
};
const dog1 = new dog("Frankie",5,"Dachshund");
console.log(dog1);

const dog2 = new dog();
dog2.name = "Bella",
dog2.age = "2",
dog2.breed = "Poodle"

console.log(dog2);